Install notmuch
===============
```
sudo aptitude install notmuch
```

Install muchsync
================
```
sudo aptitude install g++ libsqlite3-dev pkg-config libssl-dev libxapian-dev libnotmuch-dev pandoc
git clone http://www.muchsync.org/muchsync.git
cd muchsync
autoreconf -i
./configure
make
sudo make install
```

Install python-websocket
========================
```
git clone https://github.com/liris/websocket-client.git
cd websocket-client
pyhton setup.py install
```

Install msws-client
===================
```
sudo wget -O /usr/local/bin/msws-client https://gitlab.com/msws/msws-client/raw/master/msws-client
sudo chmod +x /usr/local/bin/msws-client

cat << EOF >> ~/.msws-client
EMAIL="sim6@probeta.net"
PASSWORD="password"
HOST="$(echo "$EMAIL" | cut -d@ -f2)"
URL="wss://$HOST:443"
WSDUMP="wsdump.py"
EOF
```

Run msws-client with muchsync
=============================
First time:
```
muchsync --init=mail -s msws-client ""
```
To syncronize:
```
muchsync -s msws-client ""
```
